package baseClass;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Menu 
{
	static BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
	static int a;
	
	public static void main(String args[]) throws IOException
	{

//					ROOM.JAVA
		
		System.out.println("Create room: 1-Yes /0-No \n");
		String s=br.readLine();
		a = Integer.parseInt(s);
		while(a==1)
		{
			Room roomObject=new Room();
			roomObject.getNr();
			roomObject.getBeds();
			roomObject.getFloor();
			roomObject.getPrice();
			roomObject.getFeatures();
			System.out.println("Create room: 1-Yes /0-No \n");
			s=br.readLine();
			a = Integer.parseInt(s);
		}
	
//					CUSTOMER.JAVA
		System.out.println("New customer: 1-Yes /0-No \n");
		s=br.readLine();
		a = Integer.parseInt(s);
		while(a==1){
			Customer customerObject=new Customer();
			customerObject.getName();
			customerObject.getCNP();
			customerObject.getPhoneNumber();
			customerObject.getVIP();
			customerObject.getSpecialSituatin();
			System.out.println("New customer: 1-Yes /0-No \n");
			s=br.readLine();
			a = Integer.parseInt(s);
		}
	
//					BILL.JAVA
		
	
		System.out.println("Create a bill: 1-Yes /0-No \n");
		s=br.readLine();
		a = Integer.parseInt(s);
		while(a==1)
		{
			Bill billObject =new Bill();
			billObject.getCustomerB();
			billObject.getRoom();
			billObject.getArrival();
			billObject.getCheckOut();
			System.out.println("Create a bill: 1-Yes /0-No \n");
			s=br.readLine();
			a = Integer.parseInt(s);
		}



//					REZERVATION.JAVA


		System.out.println("Create a rezervation: 1-Yes /0-No \n");
		s=br.readLine();
		a = Integer.parseInt(s);
		while(a==1)
		{
			Bill billObject =new Bill();
			billObject.getCustomerB();
			billObject.getRoom();
			billObject.getArrival();
			billObject.getCheckOut();
			System.out.println("Create a rezervation: 1-Yes /0-No \n");
			s=br.readLine();
			a = Integer.parseInt(s);
		}
	}
}
