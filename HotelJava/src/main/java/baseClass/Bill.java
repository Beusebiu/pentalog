package baseClass;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;


public class Bill{
	BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
	String s;
	public void getCustomerB() throws IOException{
			Customer customerObject=new Customer();
			customerObject.getName();
			customerObject.getCNP();
			customerObject.getPhoneNumber();
			customerObject.getVIP();
			customerObject.getSpecialSituatin();
	}
	
	public void getRoom() throws IOException{
		Room roomObject=new Room();
		roomObject.getNr();
		roomObject.getBeds();
		roomObject.getFloor();
		roomObject.getPrice();		
	}
	public void getArrival() throws IOException
	{
		System.out.println("Enter Arrival year:");
		s=br.readLine();
		int y=Integer.parseInt(s);
		while(y<2016||y>2020)
		{
			System.out.println("Wrong insert!\n Enter Arrival year:");
			s=br.readLine();
			y = Integer.parseInt(s);
		}
		
		System.out.println("Enter Arrival month:");
		s=br.readLine();
		int m=Integer.parseInt(s);
		while(m>12)
		{
			System.out.println("Wrong insert!\n Enter Arrival month:");
			s=br.readLine();
			m = Integer.parseInt(s);
		}	
		
		System.out.println("Enter Arrival day:");
		s=br.readLine();
		int d=Integer.parseInt(s);
		while(d>31)
		{
			System.out.println("Wrong insert!\n Enter Arrival day:");
			s=br.readLine();
			d = Integer.parseInt(s);
		}	
	}
	
	public void getCheckOut() throws IOException
	{
		System.out.println("Enter Check Out year:");
		s=br.readLine();
		int y=Integer.parseInt(s);
		while(y<2016||y>2020)
		{
			System.out.println("Wrong insert!\n Enter Check Out year:");
			s=br.readLine();
			y = Integer.parseInt(s);
		}
		
		System.out.println("Enter Check Out month:");
		s=br.readLine();
		int m=Integer.parseInt(s);
		while(m>12)
		{
			System.out.println("Wrong insert!\n Enter Check Out month:");
			s=br.readLine();
			m = Integer.parseInt(s);
		}	
		
		System.out.println("Enter Check Out day:");
		s=br.readLine();
		int d=Integer.parseInt(s);
		while(d>31)
		{
			System.out.println("Wrong insert!\n Enter Check Out day:");
			s=br.readLine();
			d = Integer.parseInt(s);
		}
	}
}
