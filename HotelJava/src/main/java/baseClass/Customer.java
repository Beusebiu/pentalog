package baseClass;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Customer  extends Bill{
	BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
	String s;
	
	public void getName() throws IOException
	{
		System.out.println("Insert name: ");
		br.readLine();
	}
	public void getCNP() throws IOException
	{
		System.out.println("Enter CNP: ");
		s=br.readLine();
		Double.parseDouble(s);	
	}

	public void getVIP() throws IOException{
		System.out.println("Enter V.I.P. status: 1-YES /0-NO");
		s=br.readLine();
		int a=Integer.parseInt(s);
		while(a!=1 && a!=0)
		{
			System.out.println("Wrong insert!\n");
			s=br.readLine();
			a = Integer.parseInt(s);
		}	
	}
	public void getPhoneNumber() throws IOException
	{
		System.out.println("Enter phone number: ");
		s=br.readLine();
		Double.parseDouble(s);	
	}
	
	public void getSpecialSituatin() throws IOException{
		System.out.println("Have special situatin status? 1-YES /0-NO");
		s=br.readLine();
		int a=Integer.parseInt(s);
		while(a!=1 && a!=0)
		{
			System.out.println("Wrong insert!\n");
			s=br.readLine();
			a = Integer.parseInt(s);
		}	
	}
	
	
}
